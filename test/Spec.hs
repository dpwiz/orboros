{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE NoFieldSelectors #-}
{-# LANGUAGE OverloadedRecordDot #-}

import Prelude
import Data.Time
import Test.Tasty
import Test.Tasty.HUnit

import J2000

earthElements :: KeplerElements
earthElements = fromJ2000
  [j2000|
    Semimajor axis (AU)                  1.00000011  
    Orbital eccentricity                 0.01671022   
    Orbital inclination (deg)            0.00005  
    Longitude of ascending node (deg)  -11.26064  
    Longitude of perihelion (deg)      102.94719  
    Mean Longitude (deg)               100.46435
  |]

marsElements :: KeplerElements
marsElements = fromJ2000
  [j2000|
    Semimajor axis (AU)                  1.52366231  
    Orbital eccentricity                 0.09341233   
    Orbital inclination (deg)            1.85061   
    Longitude of ascending node (deg)   49.57854  
    Longitude of perihelion (deg)      336.04084   
    Mean Longitude (deg)               355.45332
  |]

main :: IO ()
main = defaultMain $ testGroup "Distance Tests"
  [ testCase "Mars-Earth distance at 2000-01-01" $
      testDistance j2000Epoch @?= 1.849366006846124
  , testCase "Mars-Earth distance in 2012-01-01" $
      testDistance (UTCTime (fromGregorian 2012 1 1) 0) @?= 1.0406402202930918
  , testCase "Mars-Earth distance in 2025-03-05" $
      testDistance (UTCTime (fromGregorian 2025 3 5) 0) @?= 0.8934833205421147
  ]

testDistance :: UTCTime -> Double
testDistance testTime = sqrt (dot3D pos pos)
  where
    earthPos = planetPositionAtTime earthElements testTime
    marsPos = planetPositionAtTime marsElements testTime
    pos = marsPos `into` earthPos
    into (ax, ay, az) (bx, by, bz) = (ax - bx, ay - by, az - bz)
    dot3D (ax, ay, az) (bx, by, bz) = ax * bx + ay * by + az * bz

fromJ2000 :: J2000Elements -> KeplerElements
fromJ2000 J2000Elements{..} = KeplerElements{..}
  where
    argPeriapsis = longPerihelion - longAscNode
    meanAnomalyJ2000 = (meanLongitude - longPerihelion) `mod'` 360
  
-- | Orbital elements at J2000 epoch
data KeplerElements = KeplerElements
  { semiMajorAxis      :: Double  -- ^ a, in AU
  , eccentricity       :: Double  -- ^ e, dimensionless
  , inclination        :: Double  -- ^ i, in degrees
  , longAscNode        :: Double  -- ^ Ω, in degrees
  , argPeriapsis       :: Double  -- ^ ω, in degrees
  , meanAnomalyJ2000   :: Double  -- ^ M at J2000, in degrees
  } deriving (Show)

-- | Compute position at a specific UTC time
planetPositionAtTime :: KeplerElements -> UTCTime -> (Double, Double, Double)
planetPositionAtTime orb utcTime = calculatePosition orb m
  where
    timeDiffSeconds = realToFrac (diffUTCTime utcTime j2000Epoch)
    timeDiffDays = timeDiffSeconds / (24 * 3600)
    n = meanMotion orb.semiMajorAxis -- radians/day
    deltaM = n * timeDiffDays -- radians
    deltaMDeg = deltaM * 180 / pi -- degrees
    m = (orb.meanAnomalyJ2000 + deltaMDeg) `mod'` 360 -- degrees

-- | Core position calculation given orbital elements and mean anomaly
calculatePosition :: KeplerElements -> Double -> (Double, Double, Double)
calculatePosition KeplerElements{..} m = (x, y, z)
  where
    a = semiMajorAxis
    e = eccentricity
    i = inclination
    omegaBig = longAscNode
    omegaSmall = argPeriapsis

    iRad = degToRad i
    omegaBigRad = degToRad omegaBig
    omegaSmallRad = degToRad omegaSmall
    mRad = degToRad m

    eRad = solveKepler e mRad

    cosNu = (cos eRad - e) / (1 - e * cos eRad)
    sinNu = sqrt (1 - e * e) * sin eRad / (1 - e * cos eRad)

    r = a * (1 - e * e) / (1 + e * cosNu)

    xOrb = r * cosNu
    yOrb = r * sinNu

    cosOmegaBig = cos omegaBigRad
    sinOmegaBig = sin omegaBigRad
    cosI = cos iRad
    sinI = sin iRad
    cosOmegaSmall = cos omegaSmallRad
    sinOmegaSmall = sin omegaSmallRad

    p11 = cosOmegaBig * cosOmegaSmall - sinOmegaBig * sinOmegaSmall * cosI
    p12 = -cosOmegaBig * sinOmegaSmall - sinOmegaBig * cosOmegaSmall * cosI
    p21 = sinOmegaBig * cosOmegaSmall + cosOmegaBig * sinOmegaSmall * cosI
    p22 = -sinOmegaBig * sinOmegaSmall + cosOmegaBig * cosOmegaSmall * cosI
    p31 = sinOmegaSmall * sinI
    p32 = cosOmegaSmall * sinI

    x = p11 * xOrb + p12 * yOrb
    y = p21 * xOrb + p22 * yOrb
    z = p31 * xOrb + p32 * yOrb

-- | Solve Kepler's equation (M = E - e * sin E) for E
solveKepler :: Double -> Double -> Double
solveKepler e mRad = go mRad
  where
    go eN =
      let eNext = eN - (eN - e * sin eN - mRad) / (1 - e * cos eN)
      in if abs (eNext - eN) < 1e-10 then eNext else go eNext

-- | J2000 epoch (approximated as 2000-01-01 12:00 UTC)
j2000Epoch :: UTCTime
j2000Epoch = UTCTime (fromGregorian 2000 1 1) (secondsToDiffTime 43200)

-- | Solar gravitational parameter in AU^3/day^2
muSun :: Double
muSun = 2.959122082855911e-4

-- | Orbital period in days
orbitalPeriod :: Double -> Double
orbitalPeriod a = 2 * pi * sqrt (a * a * a / muSun)

-- | Mean motion in radians per day
meanMotion :: Double -> Double
meanMotion a = 2 * pi / orbitalPeriod a

-- | Modulo function to keep angles in [0, y)
mod' :: Double -> Double -> Double
mod' x y = x - y * fromIntegral (floor (x / y) :: Integer)

-- | Convert degrees to radians
degToRad :: Double -> Double
degToRad deg = deg * pi / 180
