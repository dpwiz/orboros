{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE NoFieldSelectors #-}
{-# LANGUAGE OverloadedRecordDot #-}
{-# LANGUAGE QuasiQuotes #-}

module J2000 where

import Prelude

import Control.Monad (unless)
import Language.Haskell.TH.Quote (QuasiQuoter (..))
import Language.Haskell.TH.Syntax (Lift, Q, Exp)
import Text.Parsec.String (Parser)
import Text.Parsec (parse, spaces, string, char, option, many1, satisfy)

-- | Orbital elements at J2000 epoch
data J2000Elements = J2000Elements
  { semiMajorAxis  :: Double -- ^ a, in AU
  , eccentricity   :: Double -- ^ e, dimensionless
  , inclination    :: Double -- ^ i, in degrees
  , longAscNode    :: Double -- ^ Ω, in degrees
  , longPerihelion :: Double -- ^ ω, in degrees
  , meanLongitude  :: Double -- ^ M at J2000, in degrees
  } deriving (Show, Lift)

-- | Quasiquoter for J2000 orbital elements
j2000 :: QuasiQuoter
j2000 = QuasiQuoter
  { quoteExp = parseJ2000Elements
  , quotePat = error "j2000 quasiquoter cannot be used in patterns"
  , quoteType = error "j2000 quasiquoter cannot be used in types"
  , quoteDec = error "j2000 quasiquoter cannot be used in declarations"
  }

-- | Parse J2000 elements and generate Haskell expression
parseJ2000Elements :: String -> Q Exp
parseJ2000Elements str = case parse j2000Parser "" str of
  Left err -> fail $ "Error parsing J2000 elements: " ++ show err
  Right elements -> [| elements |]

-- | Parser for J2000 elements
j2000Parser :: Parser J2000Elements
j2000Parser = do
  spaces
  semiMajorAxis <- parseNamedValue "Semimajor axis" "AU"
  eccentricity <- parseNamedValue "Orbital eccentricity" ""
  inclination <- parseNamedValue "Orbital inclination" "deg"
  longAscNode <- parseNamedValue "Longitude of ascending node" "deg"
  longPerihelion <- parseNamedValue "Longitude of perihelion" "deg"
  meanLongitude <- parseNamedValue "Mean Longitude" "deg"
  spaces
  pure J2000Elements{..}

-- | Parse a named value in the format "Name (unit) value"
parseNamedValue :: String -> String -> Parser Double
parseNamedValue name unit = do
  spaces
  string name
  spaces
  unless (null unit) $ do
    char '('
    string unit
    char ')'
    spaces
  parseNumber

-- | Parse a floating point number
parseNumber :: Parser Double
parseNumber = do
  spaces
  sign <- option 1 (char '-' >> pure (-1))
  digits <- many1 (satisfy (`elem` ("0123456789." :: [Char])))
  spaces
  pure $! sign * read digits
