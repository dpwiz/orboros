FROM fpco/stack-build:lts-16.17

RUN wget -qO - https://packages.lunarg.com/lunarg-signing-key-pub.asc | apt-key add -

RUN wget -qO /etc/apt/sources.list.d/lunarg-vulkan-bionic.list https://packages.lunarg.com/vulkan/lunarg-vulkan-bionic.list

RUN wget -q \
  -O /usr/local/bin/linuxdeploy-x86_64.AppImage \
  https://github.com/linuxdeploy/linuxdeploy/releases/download/continuous/linuxdeploy-x86_64.AppImage

RUN wget -q \
  -O /usr/local/bin/linuxdeploy-plugin-appimage-x86_64.AppImage \
  https://github.com/linuxdeploy/linuxdeploy-plugin-appimage/releases/download/continuous/linuxdeploy-plugin-appimage-x86_64.AppImage

RUN chmod +x /usr/local/bin/linuxdeploy-*

RUN apt update && apt install -y \
  vulkan-sdk \
  libsdl2-dev \
  libsdl2-mixer-dev \
  libogg-dev \
  fuse \
  libfuse-dev \
  && rm -rf /var/lib/apt/lists/*
