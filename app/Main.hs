module Main (main) where

import RIO

import Engine.App (engineMainWith)

import Stage.Loader.Setup (bootstrap)
import Stage.Main.Setup qualified as Main
import Global.Resource.Font qualified as Font
import Global.Resource.Texture.Base qualified as Base

main :: IO ()
main =
  engineMainWith handoff action
  where
    (handoff, action) =
      bootstrap
        "Orboros"
        fonts
        (splash, spinner)
        Main.loadAction
        Main.stackStage
      where
        Font.Collection{..} = Font.configs
        fonts = (small, large)

        Base.Collection{black} = Base.sources
        splash = black
        spinner = black
