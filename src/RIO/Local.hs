{-# LANGUAGE QuasiQuotes #-}

module RIO.Local
  ( module RE

  , τ
  , turn
  , fract
  , trim

  , textShow
  ) where

import RIO as RE

import Control.Category as RE ((<<<))
import Data.Bits as RE (Bits, zeroBits, (.|.), (.&.))
import Data.Coerce as RE (coerce)
import Data.List as RE (sort, sortOn)
import Data.Metrology as RE (numIn, quOf, (|+|), (|-|), (|*|), (|/|))
import Data.Tagged as RE (Tagged(..))
import Data.Units.SI.Parser as RE (si)
import Foreign.C.Types as RE (CInt)
import Foreign.Ptr as RE (castPtr)
import Foreign.Storable as RE (Storable(..))
import Geomancy as RE
import GHC.Float as RE (double2Float, float2Double)
import Physics.Orbit as RE (Quantity, Time, Distance, Angle)
import RIO.FilePath as RE ((</>))
import RIO.Text qualified as Text
import Vulkan.CStruct.Extends as RE (SomeStruct(..))
import Vulkan.NamedType as RE ((:::))
import Vulkan.Zero as RE (Zero(..))

{-# INLINE τ #-}
τ :: Floating a => a
τ = 2 * pi

{-# INLINE turn #-}
turn :: Floating a => Angle a
turn = quOf τ [si| rad |]

{-# INLINE fract #-}
fract :: Float -> Float
fract f = f - fromIntegral @Int (floor f)

{-# INLINE trim #-}
trim :: Ord a => a -> a -> a -> a
trim rMin rMax = max rMin . min rMax

textShow :: Show a => a -> Text
textShow = Text.pack . show
