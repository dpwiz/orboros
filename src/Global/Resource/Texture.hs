{-# LANGUAGE DeriveAnyClass #-}

-- {-# OPTIONS_GHC -fforce-recomp #-}

module Global.Resource.Texture
  ( Collection(..)
  , TextureCollection
  , sources
  ) where

import RIO

import GHC.Generics (Generic1)
import Resource.Collection (Generically1(..))
import Resource.Source (Source)
import Resource.Texture (Texture, Flat)

import Global.Resource.Texture.Base qualified as Base

data Collection a = Collection
  { base :: Base.Collection a
  }
  deriving stock (Show, Functor, Foldable, Traversable, Generic1)
  deriving Applicative via (Generically1 Collection)

type TextureCollection = Collection (Int32, Texture Flat)

sources :: Collection Source
sources = Collection
  { base = Base.sources
  }
