{-# LANGUAGE DeriveAnyClass #-}

-- {-# OPTIONS_GHC -fforce-recomp #-}

module Global.Resource.Sound
  ( Collection(..)
  , Sources
  , configs
  , indices
  ) where

import RIO

import GHC.Generics (Generic1)
import Resource.Collection (Generically1(..), enumerate)
import Resource.Opus qualified as Opus
import Resource.Source qualified as Source
import Resource.Static qualified as Static

data Collection a = Collection
  { bg     :: a
  , click  :: a
  , detect :: a
  , nope   :: a
  , score  :: a
  , thrust :: a
  }
  deriving stock (Show, Functor, Foldable, Traversable, Generic1)
  deriving Applicative via (Generically1 Collection)

type Sources = Collection Opus.Source

Static.filePatterns Static.Files "data/sound"

configs :: Collection Opus.Config
configs = Collection
  { bg = Opus.Config
      { gain        = 0.5
      , loopingMode = True
      , byteSource  = Source.File Nothing BG_OPUS
      }
  , click = Opus.Config
      { gain        = 1.0
      , loopingMode = False
      , byteSource  = Source.File Nothing CLICK_OPUS
      }
  , detect = Opus.Config
      { gain        = 1.0
      , loopingMode = False
      , byteSource  = Source.File Nothing DETECT_OPUS
      }
  , nope = Opus.Config
      { gain        = 1.0
      , loopingMode = False
      , byteSource  = Source.File Nothing NOPE_OPUS
      }
  , score = Opus.Config
      { gain        = 1.0
      , loopingMode = False
      , byteSource  = Source.File Nothing SCORE_OPUS
      }
  , thrust = Opus.Config
      { gain        = 1.0
      , loopingMode = True
      , byteSource  = Source.File Nothing THRUST_OPUS
      }
  }

indices :: Collection Int
indices = fmap fst $ enumerate configs
