{-# LANGUAGE DeriveAnyClass #-}

-- {-# OPTIONS_GHC -fforce-recomp #-}

module Global.Resource.CubeMap
  ( Collection(..)
  , CubeMapCollection
  , Ids
  , Textures
  , sources
  , indices
  , numCubes
  ) where

import RIO

import GHC.Generics (Generic1)
import Global.Resource.CubeMap.Base qualified as Base

import Resource.Collection (Generically1(..), enumerate, size)
import Resource.Static as Static
import Resource.Texture (Texture, CubeMap)
import Resource.Source (Source)
import Resource.Source qualified as Source

data Collection a = Collection
  { base     :: Base.Collection a
  , milkyway :: a
  }
  deriving stock (Show, Functor, Foldable, Traversable, Generic1)
  deriving Applicative via (Generically1 Collection)

type CubeMapCollection = Collection (Int32, Texture CubeMap)
type Ids               = Collection Int32
type Textures          = Collection (Texture CubeMap)

Static.filePatterns Static.Files "data/cubemaps"

sources :: Collection Source
sources = Collection
  { base     = Base.sources
  , milkyway = Source.File Nothing MILKYWAY_KTX2
  }

indices :: Collection Int32
indices = fmap fst $ enumerate sources

numCubes :: Num a => a
numCubes = size sources
