module Stage.Main.Event.Key
  ( callback
  , keyHandler
  ) where

import RIO

import Engine.Events.Sink (MonadSink, Sink(..))
-- import Engine.Types (StageRIO)
import Engine.Window.Key (Key(..), KeyState(..))
import Engine.Window.Key qualified as Key
import UnliftIO.Resource (ReleaseKey)

import Stage.Main.Event.Type (Event)
import Stage.Main.Event.Type qualified as Event
import Stage.Main.Scene qualified as Scene
import Stage.Main.Types (RunState(..))
import Stage.Main.World.Orbiter qualified as Orbiter

callback :: MonadSink RunState m => Sink Event RunState -> m ReleaseKey
callback = Key.callback . keyHandler

keyHandler :: MonadSink RunState m => Sink Event RunState -> Key.Callback m
keyHandler (Sink signal) keyCode keyEvent@(_mods, state, key) =
  unless (state == KeyState'Repeating) do
    logDebug $ "Key event (" <> display keyCode <> "): " <> displayShow keyEvent
    case key of
      Key'Pause | pressed ->
        signal Event.Pause
      Key'P | pressed ->
        signal Event.Pause
      Key'0 | pressed ->
        signal Event.Pause
      -- TODO: time warp
      -- Key'1 | pressed ->
      --   signal $ Event.SetWarp 1.0
      -- Key'2 | pressed ->
      --   signal $ Event.SetWarp 2.0
      -- Key'3 | pressed ->
      --   signal $ Event.SetWarp 4.0
      -- Key'4 | pressed ->
      --   signal $ Event.SetWarp 8.0

      Key'Left ->
        signal . Event.Camera . Scene.CameraAzimuth $
          if pressed then
            -60
          else
            0
      Key'Right ->
        signal . Event.Camera . Scene.CameraAzimuth $
          if pressed then
            60
          else
            0
      Key'Up ->
        signal . Event.Camera . Scene.CameraAscent $
          if pressed then
            60
          else
            0
      Key'Down ->
        signal . Event.Camera . Scene.CameraAscent $
          if pressed then
            -60
          else
            0
      Key'Equal ->
        signal . Event.Camera . Scene.CameraDistance $
          if pressed then
            -- "+" means zoom in i.e. reduce distance
            -1
          else
            0
      Key'Minus ->
        signal . Event.Camera . Scene.CameraDistance $
          if pressed then
            1
          else
            0

      Key'W | pressed ->
        signal $ Event.Orient Orbiter.Prograde
      Key'S | pressed ->
        signal $ Event.Orient Orbiter.Retrograde
      Key'A | pressed ->
        signal $ Event.Orient Orbiter.AntiNormal
      Key'D | pressed ->
        signal $ Event.Orient Orbiter.Normal
      Key'Q | pressed ->
        signal $ Event.Orient Orbiter.AntiTangent
      Key'E | pressed ->
        signal $ Event.Orient Orbiter.Tangent

      Key'Space ->
        signal $ Event.Burn pressed
      -- TODO: throttle
      -- TODO: select engine/fuel

      _ ->
        pure ()
  where
    pressed = state == KeyState'Pressed
