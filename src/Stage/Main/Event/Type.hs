module Stage.Main.Event.Type
  ( Event(..)
  ) where

import RIO

import Stage.Main.Scene qualified as Scene
import Stage.Main.World.Orbiter qualified as Orbiter

data Event
  = Camera Scene.CameraEvent
  | Pause
  | Orient Orbiter.Orientation
  | Burn Bool
