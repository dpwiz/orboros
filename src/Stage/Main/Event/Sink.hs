module Stage.Main.Event.Sink
  ( handleEvent
  ) where

import RIO

import RIO.State (gets)

import Engine.Types (StageRIO)
import Engine.Sound.Source qualified as SoundSource
import Engine.Worker qualified as Worker

import Global.Resource.Sound qualified as Sound
import Stage.Main.Event.Type (Event(..))
import Stage.Main.Scene qualified as Scene
import Stage.Main.Types (RunState(..))
import Stage.Main.World qualified as World
import Stage.Main.World.System qualified as WorldSystem

handleEvent :: Event -> StageRIO RunState ()
handleEvent = \case
  Pause -> do
    world <- gets rsOrbitSim
    atomically $ modifyTVar' (World.sActive world) not

  Camera event -> do
    timer <- gets rsCameraP
    atomically do
      Worker.modifyConfigSTM timer \cc ->
        case event of
          Scene.CameraAzimuth delta ->
            cc { Scene.ccAzimuth = delta }
          Scene.CameraAscent delta ->
            cc { Scene.ccAscent = delta }
          Scene.CameraDistance delta ->
            cc { Scene.ccDistance = delta }
      Scene.CameraControls{..} <- readTVar $ Worker.getConfig timer
      writeTVar (Worker.tActive timer) $
        any (/= 0) [ccAzimuth, ccAscent, ccDistance]

  Orient dir -> do
    world <- gets $ World.sWorld . rsOrbitSim
    atomically $
      WorldSystem.orientPlayer world dir
    gets (Sound.click . rsSounds) >>=
      SoundSource.play

  Burn active -> do
    world <- gets $ World.sWorld . rsOrbitSim
    atomically $
      WorldSystem.toggleBurn world active
    gets (Sound.thrust . rsSounds) >>=
      SoundSource.toggle active
