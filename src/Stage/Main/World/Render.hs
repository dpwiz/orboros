module Stage.Main.World.Render
  ( Instance(..)
  , TailInstances(..)
  ) where

-- import RIO.Local

import Apecs.STM.Prelude qualified as Apecs
import Render.Lit.Colored.Model qualified as LitColored

newtype Instance = Instance LitColored.InstanceAttrs
instance Apecs.Component Instance where
  type Storage Instance = Apecs.Map Instance

-- | Special container derived from player position.
newtype TailInstances = TailInstances [LitColored.InstanceAttrs]
instance Apecs.Component TailInstances where
  type Storage TailInstances = Apecs.Map TailInstances
