module Stage.Main.World.Wire
  ( Wire(..)
  , Vertex
  , Model
  , mkEdge
  ) where

-- import RIO.Local

import Apecs.STM.Prelude qualified as Apecs
import Geomancy (Vec3)
import Geomancy.Vec3 qualified as Vec3
import Resource.Buffer qualified as Buffer
import Resource.Model qualified as Model
import Render.Unlit.Colored.Model qualified as UnlitColored

newtype Wire = Wire [Vertex]
instance Apecs.Component Wire where
  type Storage Wire = Apecs.Map Wire

type Vertex = Model.Vertex Vec3.Packed UnlitColored.VertexAttrs

type Model = Model.Indexed 'Buffer.Coherent Vec3.Packed UnlitColored.VertexAttrs

mkEdge
  :: (Vec3, UnlitColored.VertexAttrs)
  -> (Vec3, UnlitColored.VertexAttrs)
  -> Wire
mkEdge (beginPos, beginCol) (endPos, endCol) =
  Wire
    [ Model.Vertex
        { vPosition = Vec3.Packed beginPos
        , vAttrs    = beginCol
        }
    , Model.Vertex
        { vPosition = Vec3.Packed endPos
        , vAttrs    = endCol
        }
    ]
