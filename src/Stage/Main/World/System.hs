module Stage.Main.World.System where

import RIO.Local

import Apecs.STM.Prelude qualified as Apecs
import Engine.Sound.Source qualified as SoundSource
import Engine.Types qualified as Engine
import Engine.Worker qualified as Worker
import Formatting qualified as F
import Geomancy.Transform qualified as Transform
import Geomancy.Vec3 qualified as Vec3
import Physics.Orbit qualified as Orbit
import Physics.Orbit.StateVectors qualified as StateVectors
import Resource.Collection (enumerate)
import RIO.Partial (toEnum)
import RIO.Set qualified as Set
import System.Random (newStdGen)
import System.Random.Stateful qualified as Random
import UnliftIO.Resource (ResourceT)

import Global.Resource.Sound qualified as Sound
import Stage.Main.UI (postMessage_)
import Stage.Main.World (DT, UIProbe(..), mkTrajectory)
import Stage.Main.World qualified as World
import Stage.Main.World.Orbiter (Orbiter(..))
import Stage.Main.World.Orbiter qualified as Orbiter
import Stage.Main.World.Render qualified as Render
import Stage.Main.World.Type (World, initWorld)
import Stage.Main.World.Wire qualified as Wire

spawn
  :: Sound.Sources
  -> UIProbe
  -> ResourceT (Engine.StageRIO env) World.Simulation
spawn soundSources uiProbe =
  World.spawnSimulation interval True initAction $
    simulationStep soundSources uiProbe dt
  where
    interval = 10_000

    initAction = do
      world <- liftIO initWorld

      atomically $ Apecs.runWith world do
        void $! spawnOrbiter
          2000
          Nothing
          Orbit.NonInclined
          0
          True
          ( Orbiter.Player
              { playerPods = [Orbiter.FuelPod]
              , playerFuel = 1.0
              }
          , Orbiter.Prograde
          )

        -- -- ISS
        -- void $! spawnOrbiter
        --   419.0
        --   (Just (0.0001929, quOf 155.9812 [si| deg |]))
        --   (Orbit.Inclined
        --     { longitudeOfAscendingNode =
        --         quOf 202.7017  [si| deg |]
        --     , inclination =
        --         quOf 51.64 [si| deg |]
        --     }
        --   )
        --   0 -- 220.3125
        --   True
        --   Orbiter.CargoPod

        -- -- Tianhe-1
        -- void $! spawnOrbiter
        --   376
        --   (Just (0.0006542, quOf 155.5128 [si| deg |]))
        --   (Orbit.Inclined
        --     { longitudeOfAscendingNode =
        --         quOf 111.8918 [si| deg |]
        --     , inclination =
        --         quOf 41.4687 [si| deg |]
        --     }
        --   )
        --   0 -- 330.4152
        --   True
        --   Orbiter.CargoPod

        -- -- Molniya
        -- void $! spawnOrbiter
        --   600
        --   (Just (0.737, quOf 270 [si| deg |]))
        --   (Orbit.Inclined
        --     { longitudeOfAscendingNode =
        --         quOf 270 [si| deg |]
        --     , inclination =
        --         quOf 63.4 [si| deg |]
        --     }
        --   )
        --   (-45)
        --   True
        --   Orbiter.FuelPod

        -- -- GEO
        -- void $! spawnOrbiter
        --   35_786
        --   Nothing
        --   Orbit.NonInclined
        --   45
        --   True
        --   Orbiter.FuelPod

        -- -- Moon-distance
        -- void $! spawnOrbiter
        --   362_600
        --   Nothing
        --   Orbit.NonInclined
        --   90
        --   True
        --   Orbiter.FuelPod

        lift $
          postMessage_ (uiMessageLog uiProbe) "OrboROS session started."

      pure world

    dt = quOf (100 * fromIntegral interval) [si| μs |]

simulationStep
  :: Sound.Sources
  -> UIProbe
  -> DT
  -> World
  -> Engine.StageRIO env ()
simulationStep soundSources uiProbe dt world = do
  -- logDebug "sim step"
  gen <- newStdGen
  (plays, stops) <- atomically do
    soundStart <- newTVar (mempty :: Set Int)
    soundStop <- newTVar (mempty :: Set Int)
    let
      ($#) sample active =
        if active then
          modifyTVar soundStart $ Set.insert (sample Sound.indices)
        else
          modifyTVar soundStop $ Set.insert (sample Sound.indices)

    Apecs.runWith world do
      Apecs.cmapM $ applyThrust dt ($#)
      Apecs.cmap $ stepOrbits dt
      Apecs.cmap updateTails
      catchPods uiProbe ($#)
      Apecs.cmapM_ $ updateUiProbe uiProbe
      Apecs.cmapM_ $ trackLaunches uiProbe dt ($#)
      void $! launchPods uiProbe gen

    started <- readTVar soundStart
    stopped <- readTVar soundStop
    pure (Set.difference started stopped, stopped)
  unless (Set.null plays) $
    SoundSource.play do
      (ix, source) <- toList (enumerate soundSources)
      guard $ Set.member ix plays
      pure source
  unless (Set.null stops) $
    SoundSource.stop do
      (ix, source) <- toList (enumerate soundSources)
      guard $ Set.member ix stops
      pure source

type PlaySound = (Sound.Collection Int -> Int) -> Bool -> STM ()

spawnOrbiter
  :: Apecs.Set World STM extras
  => "periapsis altitude" ::: Double
  -> Maybe ("ecc" ::: Double, Quantity [si| deg |] Double)
  -> Orbit.InclinationSpecifier Double
  -> "MA, deg" ::: Double
  -> "project trajectory" ::: Bool
  -> extras
  -> Apecs.SystemT World STM Apecs.Entity
spawnOrbiter periAlt mEcc inclSpec startMA calcTrajectory extras =
  Apecs.newEntity
    ( Orbiter{..}
    , Orbiter.MeanAnomaly meanAnomaly
    , if calcTrajectory then
        Just $ mkTrajectory orbit
      else
        Nothing
    , extras
    )
  where
    meanAnomaly = quOf startMA [si| deg |]

    (eccentricity, periSpec) =
      case mEcc of
        Nothing ->
          (0, Orbit.Circular)
        Just (ecc', arg) ->
          (ecc', Orbit.Eccentric arg)

    orbit = Orbit.normalizeOrbit Orbit.Orbit
      { eccentricity =
          quOf eccentricity [si| |]
      , periapsis =
          World.bodyRadius |+| quOf periAlt [si| km |]
      , inclinationSpecifier =
          inclSpec
      , periapsisSpecifier =
          periSpec
      , primaryGravitationalParameter =
          World.bodyMu
      }

    meanMotion = Orbit.meanMotion orbit

applyThrust
  :: DT
  -> PlaySound
  -> ( Orbiter.Thrust
     , Orbiter.Motion
     , Orbiter.Orientation
     )
  -> Apecs.SystemT World STM
      ( Maybe Orbiter.Thrust
      , Either
          ()
          ( Orbiter
          , Orbiter.Trajectory
          , Orbiter.MeanAnomaly
          )
     )
applyThrust dt ($#) (ot@(Orbiter.Thrust someUnits), motion, orientation) =
  if badOrbit then
    lift do
      Sound.nope $# True
      Sound.thrust $# False
      pure
        ( Nothing
        , Left ()
        )
  else
    pure
      ( Just ot
      , Right
          ( Orbiter
              { orbit = newOrbit
              , meanMotion = Orbit.meanMotion newOrbit
              }
          , mkTrajectory newOrbit
          , newMA
          )
      )
  where
    (newOrbit, newTA) =
      StateVectors.elementsFromStateVectors World.bodyMu newSV

    badOrbit = or
      [ isNaN $ Orbit.eccentricity newOrbit
      , case Orbit.periapsisSpecifier newOrbit of
          Orbit.Circular ->
            False
          Orbit.Eccentric{argumentOfPeriapsis} ->
            isNaN (argumentOfPeriapsis `numIn` [si| rad |])
      , Orbit.eccentricity newOrbit > 0.99 -- stay elliptic
      , Orbit.periapsis newOrbit |-| World.bodyRadius < World.karmanLine
      , badMA
      ]

    (newMA, badMA) =
      case Orbit.meanAnomalyAtTrueAnomaly newOrbit newTA of
        Nothing ->
          ( error "assert: not used for bad anomaly"
          , True
          )
        Just ma ->
          if isNaN (ma `numIn` [si| rad |]) then
            ( error "assert: not used with NaN mean anomaly"
            , True
            )
          else
            ( Orbiter.MeanAnomaly ma
            , False
            )

    newSV = Orbiter.motionStateVectors motion
      { Orbiter.velocity = oldVelocity + deltaV
      }

    oldVelocity = Orbiter.velocity motion
    oldPosition = Orbiter.position motion

    dv = quOf someUnits [si| m/s/s |] |*| dt

    deltaV = direction ^* double2Float (dv `numIn` [si| km/s |])
      where
        direction = case orientation of
          Orbiter.Prograde    -> prograde
          Orbiter.Retrograde  -> -prograde
          -- XXX: check directions
          Orbiter.Normal      -> -gravity
          Orbiter.AntiNormal  -> gravity
          Orbiter.Tangent     -> Vec3.cross prograde gravity
          Orbiter.AntiTangent -> Vec3.cross prograde (-gravity)

        prograde = normalize oldVelocity
        gravity = normalize oldPosition

stepOrbits
  :: DT
  -> (Orbiter, Orbiter.MeanAnomaly)
  -> (Orbiter.MeanAnomaly, Orbiter.Motion, Wire.Wire, Render.Instance)
stepOrbits dt (Orbiter{..}, Orbiter.MeanAnomaly meanAnomaly) =
  ( Orbiter.MeanAnomaly nextMA
  , Orbiter.Motion
      { Orbiter.position  = position
      , Orbiter.position' = position'
      , Orbiter.velocity  = velocity
      , Orbiter.sincePeri = Orbit.timeAtMeanAnomaly orbit meanAnomaly
      }
  , Wire.mkEdge (0, 0) (position, 0.25) -- "Altitude" bar
  , Render.Instance $
      World.orbiterScale <>
      Transform.translateV position
  )
  where
    nextMA = meanAnomaly |+| meanMotion |*| dt

    position = position' ^* World.SCALE

    position' =
      World.toVec3 . fmap double2Float . fmap (`numIn` [si| km |]) $
        StateVectors.position svs

    velocity =
      World.toVec3 . fmap double2Float . fmap (`numIn` [si| km/s |]) $
        StateVectors.velocity svs

    svs =
      StateVectors.stateVectorsAtTrueAnomaly orbit $
        Orbit.trueAnomalyAtMeanAnomaly orbit nextMA

updateTails
  :: (Orbiter.Player, Orbiter, Orbiter.MeanAnomaly)
  -> Render.TailInstances
updateTails headComponents =
  Render.TailInstances do
    (ix, _pod) <- zip [1..] playerPods
    pure $
      World.orbiterScale <>
      Transform.translateV (mkPosition ix)
  where
    ( Orbiter.Player{playerPods}
      , Orbiter{..}
      , Orbiter.MeanAnomaly meanAnomaly
      ) = headComponents

    mkPosition ix = position' Vec3.^* World.SCALE
      where
        position' =
          World.toVec3 . fmap double2Float . fmap (`numIn` [si| km |]) $
            StateVectors.position svs

        svs =
          StateVectors.stateVectorsAtTrueAnomaly orbit $
            Orbit.trueAnomalyAtMeanAnomaly orbit nextMA

        nextMA =
          meanAnomaly |-| meanMotion |*| quOf (ix * 30) [si| s |]

type PodComponents =
  ( Orbiter
  , Orbiter.Pod
  , Orbiter.MeanAnomaly
  , Orbiter.Motion
  , Orbiter.Trajectory
  )

catchPods :: UIProbe -> PlaySound -> Apecs.SystemT World STM ()
catchPods UIProbe{uiMessageLog} play = do
  Apecs.cmapM_ \(ph, playerMotion, player) -> do
    let
      Orbiter.Motion
        { position' = playerPos
        , velocity  = playerVel
        } = playerMotion

    pods <- World.collect @(Orbiter.Pod, Orbiter.Motion, Apecs.Entity) id

    let
      byDistance = sortOn fst $ filter (not . isNaN . fst) do
        (podType, podMotion, pod) <- pods
        let
          Orbiter.Motion
            { position' = podPos
            , velocity  = podVel
            } = podMotion

          pos3 = podPos - playerPos
          vel3 = podVel - playerVel

          distance = norm pos3
          relativeVel = norm vel3

        pure
          ( distance
          , (pod, podType, relativeVel)
          )

    case byDistance of
      [] ->
        pure ()
      (distance, (pod, podType, relativeVel)) : _rest -> do
        if (distance < 10_000) then do
          Apecs.set player $ Orbiter.Target
            { targetEntity   = pod
            , targetDistance = quOf distance [si| km |]
            , targetVelocity = quOf relativeVel [si| km/s |]
            }
        else
          Apecs.destroy player $ Proxy @Orbiter.Target

        when (distance < 100) do
          lift do
            play Sound.score True
            postMessage_ uiMessageLog $ F.sformat
              ( "Intercepted " <<< F.shown
              <<< " at "
              <<< F.fixed 4 <<< " km/s"
              )
              podType
              relativeVel

          let
            newPods = Orbiter.playerPods ph ++ [podType]
            maxFuel = sum do
              Orbiter.FuelPod <- newPods
              pure 1.0

          Apecs.set player ph
            { Orbiter.playerPods = newPods
            , Orbiter.playerFuel = maxFuel
            }
          Apecs.destroy pod $ Proxy @PodComponents

updateUiProbe
  :: UIProbe
  -> ( Orbiter.Player
     , ( Orbiter
       , Orbiter.Orientation
       , Maybe Orbiter.Thrust
       )
     , Apecs.Entity
     )
  -> Apecs.SystemT World STM ()
updateUiProbe UIProbe{..} (player, playerComponents, playerEntity) = do
  let
    (playerOrbiter, playerOrientation, playerThrust) = playerComponents
    newOrbiter = Just playerOrbiter
    newOrientation = Just (player, playerOrientation, playerThrust)

  lift do
    Worker.updateInputSTM uiPlayerOrbit \old ->
      if old == newOrbiter then
        Nothing
      else
        Just newOrbiter

    Worker.updateInputSTM uiPlayerOrientation \old ->
      if old == newOrientation then
        Nothing
      else
        Just newOrientation

  newTarget <- Apecs.get playerEntity >>= \case
    Nothing ->
      pure Nothing
    Just target@Orbiter.Target{targetEntity} ->
      Apecs.get targetEntity >>= \case
        Nothing ->
          -- XXX: discard orphan target?
          pure Nothing
        Just targetOrbit ->
          pure $ Just (target, targetOrbit)
  lift $
    Worker.updateInputSTM uiTarget \old ->
      if old == newTarget then
        Nothing
      else
        Just newTarget

trackLaunches
  :: UIProbe
  -> DT
  -> PlaySound
  -> (Orbiter.Launch, Orbiter, Orbiter.Pod, Apecs.Entity)
  -> Apecs.SystemT World STM ()
trackLaunches uiProbe dt ($#) (Orbiter.Launch eta, Orbiter{orbit}, podType, pod) =
  if eta <= quOf 0 [si| s |] then do
    Apecs.destroy pod $ Proxy @Orbiter.Launch
    Apecs.set pod $ mkTrajectory orbit
    lift do
      Sound.detect $# True
      postMessage_ (uiMessageLog uiProbe) $
        F.sformat
          (F.shown <<< " " <<< F.shown <<< " established orbit.")
          podType
          (Apecs.unEntity pod)
  else
    Apecs.set pod $
      Orbiter.Launch $ eta |-| dt

pattern MIN_PODS :: (Eq a, Num a) => a
pattern MIN_PODS = 2

launchPods
  :: UIProbe
  -> Random.StdGen
  -> Apecs.SystemT World STM (Random.StdGen)
launchPods uiProbe gen0 = fmap snd $ Random.runStateGenT gen0 \gen -> do
  activePods <- fmap length . lift $ World.collect \(_ :: Orbiter.Pod) -> ()
  when (activePods < MIN_PODS) do
    periAltAlpha <- Random.uniformDouble01M gen
    mEcc <- do
      circular <- Random.uniformM gen
      if circular then
        pure Nothing
      else do
        ecc <- Random.uniformDouble01M gen
        deg <- Random.uniformRM (0, 359) gen
        pure $ Just (1/32 + ecc * ecc, quOf deg [si| deg |])

    inclSpec <- do
      inclined <- Random.uniformM gen
      if inclined then do
        loan <- Random.uniformRM (0, 359) gen
        incl <- Random.uniformRM (-90, 90) gen
        pure Orbit.Inclined
          { longitudeOfAscendingNode =
              quOf loan [si| deg |]
          , inclination =
              quOf incl [si| deg |]
          }
      else
        pure Orbit.NonInclined

    startMAdeg <- Random.uniformRM (-359, 359) gen

    podType <- fmap (toEnum @Orbiter.Pod) $ Random.uniformRM
      ( fromEnum @Orbiter.Pod minBound
      , fromEnum @Orbiter.Pod maxBound
      )
      gen

    arrivalMins <- Random.uniformRM (15, 30) gen

    lift do
      void $! spawnOrbiter
        (periAltAlpha * 500 + (1 - periAltAlpha) * 10_000)
        mEcc
        inclSpec
        startMAdeg
        False
        ( podType
        , Orbiter.Launch $ quOf arrivalMins [si| min |]
        )
      lift $
        postMessage_ (uiMessageLog uiProbe) $
          F.sformat
            ( "New " <<< F.shown <<<
              " launched. ETA: " <<< F.fixed 0 <<<
              " minutes."
            )
            podType
            arrivalMins

-- * Events

orientPlayer :: World -> Orbiter.Orientation -> STM ()
orientPlayer world dir = do
  Apecs.runWith world do
    Apecs.cmap \Orbiter.Player{} ->
      dir

toggleBurn :: World -> Bool -> STM ()
toggleBurn world active =
  Apecs.runWith world do
    Apecs.cmap \Orbiter.Player{} ->
      if active then
        Just $ Orbiter.Thrust 10.0
      else
        Nothing
