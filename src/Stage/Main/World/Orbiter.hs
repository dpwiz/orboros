module Stage.Main.World.Orbiter where

import RIO.Local

import Apecs.STM.Prelude qualified as Apecs
import Linear qualified
import Physics.Orbit (Orbit(..))
import Physics.Orbit qualified as Orbit
import Physics.Orbit.StateVectors qualified as StateVectors

import Stage.Main.World.Wire qualified as Wire

data Orbiter = Orbiter
  { orbit      :: Orbit Double
  , meanMotion :: Quantity [si| rad / s |] Double
  }
  deriving (Eq, Show)

instance Apecs.Component Orbiter where
  type Storage Orbiter = Apecs.Map Orbiter

newtype MeanAnomaly = MeanAnomaly (Quantity [si| rad |] Double)

instance Apecs.Component MeanAnomaly where
  type Storage MeanAnomaly = Apecs.Map MeanAnomaly

-- TODO: use dvec3
data Motion = Motion
  { position  :: Vec3
  , position' :: Vec3 -- unscaled position in km
  , velocity  :: Vec3
  , sincePeri :: Orbit.Time Double
  }
  deriving (Eq, Show)

instance Apecs.Component Motion where
  type Storage Motion = Apecs.Map Motion

motionStateVectors :: Motion -> StateVectors.StateVectors Double
motionStateVectors Motion{..} =
  StateVectors.StateVectors
    { position =
        fmap (`quOf` [si| km |]) $
          fmap float2Double $ toV3 position'
    , velocity =
        fmap (`quOf` [si| km/s |]) $
          fmap float2Double $ toV3 velocity
    }
  where
    toV3 (WithVec3 x nz y) = Linear.V3 x y (-nz)

newtype Trajectory = Trajectory [Wire.Vertex]
instance Apecs.Component Trajectory where
  type Storage Trajectory = Apecs.Map Trajectory

data Orientation
  = Prograde
  | Retrograde
  | Normal
  | AntiNormal
  | Tangent
  | AntiTangent
  deriving (Eq, Ord, Bounded, Enum, Show)

instance Apecs.Component Orientation where
  type Storage Orientation = Apecs.Map Orientation

newtype Direction = Direction Quaternion
instance Apecs.Component Direction where
  type Storage Direction = Apecs.Map Direction

newtype Thrust = Thrust Double -- XXX: units?
  deriving (Eq, Show)
instance Apecs.Component Thrust where
  type Storage Thrust = Apecs.Map Thrust

data Player = Player
  { playerPods :: [Pod]
  , playerFuel :: Float
  }
  deriving (Eq, Show, Generic)

instance Apecs.Component Player where
  type Storage Player = Apecs.Unique Player

data Target = Target
  { targetEntity   :: Apecs.Entity
  , targetDistance :: Quantity [si| km |] Float
  , targetVelocity :: Quantity [si| m/s |] Float
  }
  deriving (Eq, Show, Generic)

instance Apecs.Component Target where
  type Storage Target = Apecs.Unique Target

data Pod
  = CargoPod
  | FuelPod
  deriving (Eq, Ord, Show, Enum, Bounded)

instance Apecs.Component Pod where
  type Storage Pod = Apecs.Map Pod

data Launch = Launch
  { launchTime :: Quantity [si| s |] Double
  }
  deriving (Eq, Show)

instance Apecs.Component Launch where
  type Storage Launch = Apecs.Map Launch
