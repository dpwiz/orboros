module Stage.Main.Types
  ( Stage
  , Frame

  , Assets
  , FrameResources(..)
  , RunState(..)
  ) where

import RIO.Local

import Engine.Events qualified as Events
import Render.Basic qualified as Basic
import Render.DescSets.Set0 qualified as Set0
import Render.DescSets.Sun (Sun)
import Resource.Buffer qualified as Buffer
import UnliftIO.Resource qualified as Resource
import Vulkan.Core10 qualified as Vk

import Global.Resource.Combined qualified as Combined
import Global.Resource.CubeMap qualified as CubeMap
import Global.Resource.Font (FontCollection)
import Global.Resource.Sound qualified as Sound
import Stage.Main.Event.Type (Event)
import Stage.Main.Resource.Model qualified as GameModel
import Stage.Main.Scene qualified as Scene
import Stage.Main.UI (UI)
import Stage.Main.UI qualified as UI
import Stage.Main.World qualified as World

type Stage = Basic.Stage FrameResources RunState

type Frame = Basic.Frame FrameResources

type Assets =
  ( FontCollection
  , Combined.Textures
  , CubeMap.Textures
  , GameModel.Collection
  , Sound.Sources
  )

data FrameResources = FrameResources
  { frScene   :: Set0.FrameResource '[Set0.Scene]
  , frSceneUi :: Set0.FrameResource '[Set0.Scene]

  , frSunDescs :: Tagged '[Sun] (Vector Vk.DescriptorSet)
  , frSunData  :: Buffer.Allocated 'Buffer.Coherent Sun

  , frUI :: UI.Observer

  , frWorld :: World.Observer
  }

data RunState = RunState
  { rsEvents      :: Maybe (Events.Sink Event RunState)
  , rsAssets      :: Assets

  -- , rsProjectionP :: Camera.ProjectionProcess 'Camera.Perspective
  , rsCameraP     :: Scene.CameraProcess

  -- , rsCursorPos :: Worker.Var Vec2
  -- , rsCursorP   :: Worker.Merge Vec2

  , rsSceneP :: Scene.Process
  -- , rsSceneV :: Scene.InputVar

  , rsSceneUiP :: Scene.Process

  -- XXX: present in assets
  , rsModels :: GameModel.Collection
  , rsSounds :: Sound.Sources

  , rsUI :: UI

  , rsOrbitSim :: World.Simulation
  }
