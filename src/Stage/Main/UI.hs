module Stage.Main.UI
  ( UI(..)
  , spawn

  , postMessage_
  -- , postMessageRare
  , postMessage

  , Observer(..)
  , newObserver
  , observe
  , readObserved
  ) where

import RIO.Local

import Control.Monad.Trans.Resource (ResourceT)
import Engine.Types qualified as Engine
-- import Engine.UI.Layout qualified as Layout
import Engine.UI.Message qualified as Message
import Engine.Vulkan.Types (HasVulkan)
import Engine.Worker qualified as Worker
import Geomancy.Layout qualified as Layout
import Geomancy.Layout.Alignment qualified as Alignment
import Geomancy.Layout.Box (Box(..))
import Geomancy.Layout.Box qualified as Box
import Formatting qualified as F
import Physics.Orbit qualified as Orbit
import Resource.Buffer qualified as Buffer
import Resource.Combined.Textures qualified as CombinedTextures
import RIO.Vector.Storable qualified as Storable
import RIO.List (mapAccumL)
import Vulkan.Core10 qualified as Vk

import Global.Resource.Combined qualified as Combined
import Global.Resource.Font (FontCollection)
import Global.Resource.Font qualified as GlobalFont
import Stage.Main.World qualified as World
import Stage.Main.World.Orbiter qualified as Orbiter

data UI = UI
  { messageLines   :: [Worker.Var (Float, Vec4, Text)]
  , messageWorkers :: [Message.Process]
  , messageBgP     :: Worker.Merge (Storable.Vector Transform)

  , topMessageBgP  :: Worker.Merge (Storable.Vector Transform)
  , playerOrbiter  :: World.UIProbe
  , playerOrbiterP :: [Message.Process]
  }

data MkMessage where
  MkMessage
    :: Worker.Var a
    -> (a -> Text)
    -> (Message.Input -> Message.Input)
    -> MkMessage

spawn
  :: FontCollection
  -> Worker.Merge Box
  -> ResourceT (Engine.StageRIO env) UI
spawn fonts screenP = do
  -- Top HUD box

  topMessageBoxP <- Worker.spawnMerge1 (Layout.placeSize Alignment.centerTop $ vec2 1024 96) screenP

  topMessagePaddedP <- Worker.spawnMerge1 (Box.resize (-8)) topMessageBoxP

  -- HUD probe

  uiPlayerOrientation <- Worker.newVar Nothing
  uiPlayerOrbit <- Worker.newVar Nothing
  uiTarget <- Worker.newVar Nothing

  let
    mkPlayerText (Orbiter.Player{playerPods}, orientation, mThrust) =
      F.sformat
        ( "Attachments: " <<< F.shown <<< " | " <<<
          "Orientation: " <<< F.shown <<< " | " <<<
          "Thrust: " <<< F.maybed "(offline)" formatThrust
          -- TODO: fuel
        )
        playerPods
        orientation
        mThrust

    formatThrust =
      F.accessed
        (\(Orbiter.Thrust t) -> t)
        (F.fixed 1)

    mkOrbiterText Orbiter.Orbiter{orbit=Orbit.Orbit{..}, meanMotion} =
      F.sformat
        ( "Periapsis: " <<< F.fixed 1 <<< " km " <<<
          formatPeriSpec <<< " " <<<
          formatInclination <<< " " <<<
          "Period: " <<< F.fixed 1 <<< " h"
        )
        (periapsis `numIn` [si| km |])
        (periapsisSpecifier, eccentricity)
        inclinationSpecifier
        ((turn |/| meanMotion) `numIn` [si| h |])
      where
        formatPeriSpec = F.later \case
          (Orbit.Circular, _zeroes) ->
            "Circular"
          (Orbit.Eccentric{argumentOfPeriapsis}, ecc) ->
            F.bformat
              ("Eccentric " <<< F.fixed 3 <<< " " <<< F.fixed 1 <<< "°")
              ecc
              (argumentOfPeriapsis `numIn` [si| deg |])

        formatInclination = F.later \case
          Orbit.NonInclined ->
            "Non-inclined"
          Orbit.Inclined{..} ->
            F.bformat
              ( "Inclined " <<< F.fixed 2 <<< "°, " <<<
                "AN lon.: " <<< F.fixed 2 <<< "°"
              )
              (inclination `numIn` [si| deg |])
              (longitudeOfAscendingNode `numIn` [si| deg |])

    mkTargetText (Orbiter.Target{..}, _orbiter) =
      F.sformat
        ("Target: " <<< F.fixed 1 <<< " km @ " <<< F.fixed 1 <<< " km/s")
        (targetDistance `numIn` [si| km |])
        (targetVelocity `numIn` [si| km/s |])

  playerOrbiterP <- spawnMessages topMessagePaddedP
    [ MkMessage
        uiPlayerOrientation
        (maybe mempty mkPlayerText)
        ( \m -> m
            { Message.inputOrigin = Alignment.leftTop
            }
        )
    , MkMessage
        uiPlayerOrbit
        (maybe mempty mkOrbiterText)
        ( \m -> m
            { Message.inputOrigin = Alignment.leftMiddle
            }
        )
    , MkMessage
        uiTarget
        (maybe mempty mkTargetText)
        ( \m -> m
            { Message.inputOrigin = Alignment.leftBottom
            }
        )
    ]

  topMessageBgP <-
    Worker.spawnMerge1 (Storable.singleton . Box.mkTransform) topMessageBoxP

  -- Message log
  messageBoxP <- Worker.spawnMerge1
    (Layout.placeSize Alignment.centerBottom $ vec2 1024 $ sum lineSizes)
    screenP

  lineBoxes <- spawnLines messageBoxP

  messageLines <- for lineSizes \lineSize ->
    Worker.newVar (lineSize, 1, "")

  messageWorkers <- for (zip lineBoxes messageLines) \(lineBoxP, messageVar) ->
    Message.spawnFromR lineBoxP messageVar \(size, color, text) ->
      messageLine size color text

  messageBgP <- Worker.spawnMerge1 (Storable.singleton . Box.mkTransform) messageBoxP

  let playerOrbiter = World.UIProbe{uiMessageLog=messageLines, ..}

  pure UI{..}
  where
    spawnMessages
      :: Worker.Merge Box
      -> [MkMessage]
      -> ResourceT (Engine.StageRIO env) [Message.Process]
    spawnMessages boxP mkMessages = do
      for mkMessages \(MkMessage var mkText withMessage) ->
        Message.spawnFromR boxP var \probe ->
          withMessage (messageLine 16 1 $ mkText probe)

    messageLine size color text = Message.Input
      { inputText         = text
      , inputOrigin       = Alignment.centerBottom
      , inputSize         = size
      , inputColor        = color
      , inputFont         = GlobalFont.small fonts
      , inputFontId       = GlobalFont.small $ CombinedTextures.fonts Combined.indices
      , inputOutline      = vec4 0 0 0 1
      , inputOutlineWidth = 2/32
      , inputSmoothing    = 4/32
      }

lineSizes :: [Float]
lineSizes = replicate 5 22

spawnLines :: MonadUnliftIO m => Worker.Merge Box -> ResourceT m [Worker.Merge Box]
spawnLines parentP = sequence . snd $ mapAccumL spawnLine 0 lineSizes
  where
    spawnLine offset height =
      ( offset + height
      , Worker.spawnMerge1 (place offset height) parentP
      )

    place offset height parent =
      Box.withTRBL parent \t r _b l ->
        Box.fromTRBL (t + offset) r (t + offset + height) l

numLines :: Int
numLines = length lineSizes

-- * Updates

type MessageLog = [Worker.Var MessageLine]
type MessageLine = (Float, Vec4, Text)

postMessage :: MessageLog -> Float -> Vec4 -> Text -> STM ()
postMessage lineVars size color text = do
  current <- traverse (fmap Worker.vData . readTVar) lineVars
  let new = take numLines $ (size, color, text) : map fade current
  for_ (zip lineVars new) \(var, input) ->
    Worker.pushInputSTM var (const input)
  where
    fade (s, c, t) = (max 16 $ s - 1, c * 0.9, t)

-- postMessageRare :: UI -> GameStuff.Rarity -> Text -> STM ()
-- postMessageRare ui rarity text =
--   postMessage ui (fromIntegral $ 16 + fromEnum rarity) (rareColor rarity) text

postMessage_ :: MessageLog -> Text -> STM ()
postMessage_ lineVars = postMessage lineVars 16 0.9

-- rareColor :: GameStuff.Rarity -> Vec4
-- rareColor = \case
--   GameStuff.Common    -> rgbI 202 202 202
--   GameStuff.Uncommon  -> rgbI 128 207  63
--   GameStuff.Rare      -> rgbI  47 213   1
--   GameStuff.Epic      -> rgbI 189  63 250
--   GameStuff.Legendary -> rgbI 253 174  83
--   GameStuff.Mythic    -> rgbI 252 221 121
--   where
--     rgbI r g b = vec4 (r/255) (g/255) (b/255) 1

-- * Output

data Observer = Observer
  { topMessages   :: [Message.Observer]
  , topMessagesBg :: BgObserver

  , messageLog   :: [Message.Observer]
  , messageLogBg :: BgObserver
  }

type BgBuffer = Buffer.Allocated 'Buffer.Coherent Transform
type BgObserver = Worker.ObserverIO BgBuffer

newObserver :: UI -> ResourceT (Engine.StageRIO st) Observer
newObserver UI{..} = do
  topMessages <- traverse (const $ Message.newObserver 256) playerOrbiterP
  topMessagesBg <- Buffer.newObserverCoherent
    "topMessagesBg"
    Vk.BUFFER_USAGE_VERTEX_BUFFER_BIT
    1
    mempty

  messageLog <- traverse (const $ Message.newObserver 256) messageWorkers
  messageLogBg <- Buffer.newObserverCoherent
    "messageLogBg"
    Vk.BUFFER_USAGE_VERTEX_BUFFER_BIT
    1
    mempty

  pure Observer{..}

observe
  :: ( HasVulkan env
     )
  => UI
  -> Observer
  -> RIO env ()
observe UI{..} Observer{..} = do
  traverse_ (uncurry Message.observe) $
    zip playerOrbiterP topMessages
  -- Message.observe topMessageP topMessages
  Buffer.observeCoherentResize_ topMessageBgP topMessagesBg

  traverse_ (uncurry Message.observe) $
    zip messageWorkers messageLog
  Buffer.observeCoherentResize_ messageBgP messageLogBg

readObserved
  :: MonadUnliftIO m
  => Observer
  -> m ([Message.Buffer], BgBuffer, BgBuffer)
readObserved Observer{..} = (,,)
  <$> traverse Worker.readObservedIO (topMessages <> messageLog)
  <*> Worker.readObservedIO topMessagesBg
  <*> Worker.readObservedIO messageLogBg
