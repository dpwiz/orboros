{-# LANGUAGE OverloadedLists #-}

module Stage.Main.Setup
  ( loadAction
  , stackStage
  ) where

import RIO.Local

import Control.Monad.Trans.Resource (ResourceT)
import Engine.Camera qualified as Camera
import Engine.Events qualified as Events
import Engine.Sound.Device qualified as SoundDevice
import Engine.Sound.Source qualified as SoundSource
import Engine.Stage.Component qualified as Stage
import Engine.Types (StackStage(..), StageRIO)
import Engine.Types qualified as Engine
import Engine.Vulkan.Swapchain qualified as Swapchain
import Engine.Vulkan.Types (Queues)
import Engine.Worker qualified as Worker
import Render.Basic qualified as Basic
import Render.DescSets.Set0 qualified as Set0
import Render.DescSets.Sun qualified as Sun
import Render.Samplers qualified as Samplers
import Render.ShadowMap.RenderPass qualified as ShadowPass
import Resource.Buffer qualified as Buffer
import Resource.Combined.Textures qualified as CombinedTextures
import Resource.CommandBuffer (withPools)
import Resource.Font qualified as Font
import Resource.Image qualified as Image
import Resource.Opus qualified as Opus
import Resource.Region qualified as Region
import Resource.Texture.Ktx2 qualified as Ktx2
import RIO.State (gets, modify')
import UnliftIO.Resource qualified as Resource
import Vulkan.Core10 qualified as Vk

import Global.Resource.Combined qualified as Combined
import Global.Resource.CubeMap qualified as CubeMap
import Global.Resource.Font qualified as GlobalFont
import Global.Resource.Sound qualified as Sound
import Global.Resource.Texture qualified as Textures
import Stage.Main.Event.Key qualified as Key
import Stage.Main.Event.Sink (handleEvent)
import Stage.Main.Render qualified as Render
import Stage.Main.Resource.Model qualified as Model
import Stage.Main.Scene qualified as Scene
import Stage.Main.Types (Assets, FrameResources(..), RunState(..), Stage)
import Stage.Main.UI qualified as UI
import Stage.Main.World qualified as World
import Stage.Main.World.System qualified as WorldSystem

loadAction
  :: (Text -> Engine.StageSetupRIO ())
  -> Engine.StageSetupRIO (Resource.ReleaseKey, Assets)
loadAction update' =
  withPools \pools -> Region.run do
    let update = lift . update'

    update "Loading sounds"
    soundDevice <- Region.local SoundDevice.allocate
    !sounds <- Region.local $
      SoundSource.allocateCollectionWith (Opus.load soundDevice) Sound.configs
    SoundSource.play (Sound.bg sounds)
    -- let
    --   update msg = do
    --     SoundSource.play (Sound.detect sounds)
    --     update' msg

    update "Loading fonts"
    fonts <- traverse (Font.allocate pools) GlobalFont.configs

    update "Loading textures"
    textures <- traverse (Ktx2.load pools) Textures.sources

    update "Loading cubemaps"
    cubemaps <- traverse (Ktx2.load pools) CubeMap.sources

    update "Loading models"
    models <- Model.allocateCollection pools

    update "Done"

    pure
      ( fmap Font.container fonts
      , CombinedTextures.Collection
          { textures = textures
          , fonts    = fmap Font.texture fonts
          }
      , cubemaps
      , models
      , sounds
      )

stackStage :: (Resource.ReleaseKey, Assets) -> StackStage
stackStage = StackStage . stage

stage :: (Resource.ReleaseKey, Assets) -> Stage
stage assets' = Stage.assemble "Main" rendering resources (Just scene)
  where
    rendering = Stage.Rendering
      { rAllocateRP = Basic.allocate_
      , rAllocateP = \swapchain rps -> do
          samplers <- Samplers.allocate (Swapchain.getAnisotropy swapchain)
          let sceneBinds = Set0.mkBindings samplers Combined.sources CubeMap.sources 1
          Basic.allocatePipelines sceneBinds (Swapchain.getMultisample swapchain) rps
      }

    resources = Stage.Resources
      { rInitialRS = initialRunState assets'
      , rInitialRR = initialFrameResources (snd assets')
      }

    scene = Stage.Scene
      { scBeforeLoop = do
          -- cursorWindow <- gets rsCursorPos
          -- cursorCentered <- gets rsCursorP

          sink <- Region.local $ Events.spawn
            handleEvent
            [ Key.callback
            -- , CursorPos.callback cursorWindow
            -- , MouseButton.callback cursorCentered MouseButton.clickHandler
            ]

          modify' \rs -> rs
            { rsEvents = Just sink
            }
      , scUpdateBuffers = Render.updateBuffers
      , scRecordCommands = Render.recordCommands
      }

initialRunState :: (Resource.ReleaseKey, Assets) -> StageRIO env (Resource.ReleaseKey, RunState)
initialRunState (assetKey, rsAssets) = Region.run do
  Region.attach assetKey

  perspective <- Camera.spawnPerspective
  ortho <- Camera.spawnOrthoPixelsCentered

  screenBoxP <- Camera.trackOrthoPixelsCentered

  rsCameraP <- Scene.spawnCamera

  rsSceneP <- Worker.spawnMerge2 Scene.mkScene perspective rsCameraP

  rsSceneUiP <- Worker.spawnMerge1 Scene.mkSceneUi ortho

  rsUI <- UI.spawn fonts screenBoxP

  rsOrbitSim <- WorldSystem.spawn rsSounds (UI.playerOrbiter rsUI)

  pure RunState{rsEvents=Nothing, ..}
  where
    (fonts, _textures, _cubemaps, rsModels, rsSounds) = rsAssets

initialFrameResources
  :: Assets
  -> Queues Vk.CommandPool
  -> Basic.RenderPasses
  -> Basic.Pipelines
  -> ResourceT (Engine.StageRIO RunState) FrameResources
initialFrameResources assets _pools passes pipelines = do
  lightsData <- Region.local $
    Buffer.allocateCoherent
      (Just "lightsData")
      Vk.BUFFER_USAGE_UNIFORM_BUFFER_BIT
      1
      Scene.staticLights

  frScene <- Set0.allocate
    (Basic.getSceneLayout pipelines)
    combinedTextures
    cubemaps
    (Just lightsData)
    [ Image.aiImageView . ShadowPass.smDepthImage $
        Basic.rpShadowPass passes
    ]
    Nothing

  frSceneUi <- Set0.allocate
    (Basic.getSceneLayout pipelines)
    combinedTextures
    Nothing
    Nothing
    mempty
    Nothing

  -- TODO: extract to Sun.allocateStatic
  (frSunDescs, frSunData) <- Sun.createSet0Ds (Basic.getSunLayout pipelines)
  Buffer.updateCoherent Scene.staticLights frSunData

  ui <- gets rsUI
  frUI <- UI.newObserver ui

  frWorld <- World.newObserver

  pure FrameResources{..}
  where
    (_fonts, combinedTextures, cubemaps, _models, _sounds) = assets
