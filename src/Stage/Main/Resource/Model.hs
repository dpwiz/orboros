{-# LANGUAGE OverloadedLists #-}

module Stage.Main.Resource.Model
  ( Collection(..)
  , allocateCollection

  , allocateQuadColored
  ) where

import RIO.Local

import Engine.Vulkan.Types (MonadVulkan, Queues)
import Geomancy.Transform qualified as Transform
import Geomancy.Vec3 qualified as Vec3
import Geometry.Icosphere qualified as Icosphere
import Geometry.Quad qualified as Quad
import Render.Lit.Colored.Model qualified as LitColored
import Render.Unlit.Colored.Model qualified as UnlitColored
import Resource.Buffer qualified as Buffer
import Resource.Model qualified as Model
import UnliftIO.Resource qualified as Resource
import UnliftIO.Resource (ResourceT)
import Vulkan.Core10 qualified as Vk

import Stage.Main.Resource.Model.Planet qualified as Planet
import Stage.Main.World (pattern SCALE)

data Collection = Collection
  { shroud :: UnlitColored.Model 'Buffer.Staged

  , zeroTransform :: Buffer.Allocated 'Buffer.Staged Transform

  , planet     :: LitColored.Model 'Buffer.Staged
  , planetInst :: Buffer.Allocated 'Buffer.Staged LitColored.InstanceAttrs

  , playerHead :: LitColored.Model 'Buffer.Staged
  , playerTail :: LitColored.Model 'Buffer.Staged
  , fuelPod    :: LitColored.Model 'Buffer.Staged
  , cargoPod   :: LitColored.Model 'Buffer.Staged
  }

allocateCollection
  :: MonadVulkan env m
  => Queues Vk.CommandPool
  -> ResourceT m Collection
allocateCollection pools = do
  shroud <- allocateQuadColored pools $ vec4 0 0 0 0.9

  zeroTransform <-
    Buffer.createStaged
      (Just "zeroTransform")
      pools
      Vk.BUFFER_USAGE_VERTEX_BUFFER_BIT
      1
      [mempty]
  Buffer.register zeroTransform

  {-
     N,        num,   pos size,    attr size,    indices,    ix size
    (5, (   20_472,    245_664,   1_064_544), (   61_440,    245_760))
    (6, (   81_912,    982_944,   4_259_424), (  245_760,    983_040))
    (7, (  327_672,  3_932_064,  17_038_944), (  983_040,  3_932_160))
    (8, (1_310_712, 15_728_544,  68_157_024), ( 3932_160, 15_728_640))
    (9, (5_242_872, 62_914_464, 272_629_344), (15728_640, 62_914_560))

    (10, 20971512, ??....)
  -}
  planet <- Planet.allocate pools 7
  -- Buffer.register planet
  planetInst <-
    Buffer.createStaged
      (Just "planetInst")
      pools
      Vk.BUFFER_USAGE_VERTEX_BUFFER_BIT
      1
      [Transform.scale $ 6371 * SCALE]
  Buffer.register planetInst

  playerHead <- allocateIcosphereLitColored pools 3 (vec2 1 0.5) (vec4 0 1 0 1)
  playerTail <- allocateIcosphereLitColored pools 3 (vec2 0.9 0.75) (vec4 0 1 0 1)
  cargoPod <- allocateIcosphereLitColored pools 3 (vec2 0 0.3) (vec4 1 1 1 1)
  fuelPod <- allocateIcosphereLitColored pools 3 (vec2 0 0.8) (vec4 1 1 0 1)

  pure Collection{..}

allocateIcosphereLitColored
  :: ( MonadVulkan env m
     , Resource.MonadResource m
     )
  => Queues Vk.CommandPool
  -> Natural
  -> Vec2
  -> Vec4
  -> m (LitColored.Model 'Buffer.Staged)
allocateIcosphereLitColored pools subdivs mr color = do
  let (positions, attrs, indices) = someModel
  model <- Model.createStaged (Just "IcosphereLitColored") pools positions attrs indices
  Model.registerIndexed_ model
  pure model
  where
    someModel =
      Icosphere.generateIndexed
        subdivs
        mkInitialAttrs
        mkMidpointAttrs
        mkVertices

    mkInitialAttrs :: Vec3 -> ()
    mkInitialAttrs _pos = ()

    mkMidpointAttrs :: Float -> Vec3 -> () -> () -> ()
    mkMidpointAttrs _scale _midPos _attr1 _attr2 = ()

    mkVertices points _faces = do
      (rawPos, ()) <- points
      let normPos = Vec3.Packed $ normalize rawPos
      pure
        ( normPos
        , LitColored.VertexAttrs
            { vaBaseColor         = color
            , vaEmissiveColor     = 0
            , vaMetallicRoughness = mr
            , vaNormal            = normPos
            }
        )

allocateQuadColored
  :: ( MonadVulkan context m
     , Resource.MonadResource m
     )
  => Queues Vk.CommandPool
  -> Vec4
  -> m (UnlitColored.Model 'Buffer.Staged)
allocateQuadColored pools color = do
  quad <- Model.createStagedL
    (Just $ "QuadColored(" <> fromString (show color) <>")")
    pools
    (Quad.toVertices $ Quad.coloredQuad color)
    Nothing

  Model.registerIndexed_ quad
  pure quad
