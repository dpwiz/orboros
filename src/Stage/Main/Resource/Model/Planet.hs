{-# LANGUAGE Strict #-}

module Stage.Main.Resource.Model.Planet
  ( allocate
  ) where

import RIO.Local

import Engine.Vulkan.Types (MonadVulkan, Queues)
import Geomancy.Vec3 qualified as Vec3
import Geomancy.Vec4 qualified as Vec4
import Geomancy.Interpolate (linear)
import Geometry.Icosphere qualified as Icosphere
import Render.Lit.Colored.Model qualified as LitColored
import Resource.Buffer qualified as Buffer
import Resource.Model qualified as Model
import RIO.Vector.Storable qualified as Storable
import UnliftIO.Resource qualified as Resource
import Vulkan.Core10 qualified as Vk

allocate
  :: ( MonadVulkan env m
     , Resource.MonadResource m
     )
  => Queues Vk.CommandPool
  -> Natural
  -> m (LitColored.Model 'Buffer.Staged)
allocate pools details = do
  model <- Model.createStaged (Just "Planet") pools (Storable.convert pv) (Storable.convert av) iv
  Model.registerIndexed_ model
  pure model
  where
    (pv, av, iv) =
      Icosphere.generateIndexed
        details
        mkInitialAttrs
        mkMidpointAttrs
        (mkVertices details)

    mkInitialAttrs = hash33

    mkMidpointAttrs scale midPos attr1 attr2 =
      linear
        (linear attr1 attr2 0.5)
        (hash33 midPos)
        (scale * scale)

mkVertices
  :: Natural
  -> Vector (Vec3, Vec3)
  -> [face Int]
  -> Vector (Vec3.Packed, LitColored.VertexAttrs)
mkVertices details points _faces = do
  (vPosition, noise') <- points -- Vector.slice 0 finalPoints points
  -- Face ia ib ic <- faces
  -- (vPosition, noise') <- map (points !) [ia, ib, ic]
  let
    normPos = normalize vPosition

    WithVec3 alpha' beta' gamma' = noise'
    WithVec3 alpha beta gamma = clean ^* (min123 / max123)
      where
        WithVec3 n1 n2 n3 = noise'
        min123 = max (1/2048) $ min n1 (min n2 n3)
        max123 = max n1 (max n2 n3)
        clean = noise' - vec3 min123 min123 min123

    surface =
      colorA ^* (alpha' / 3) +
      colorB ^* (beta' / 3) +
      colorC ^* (gamma' / 3)

    isGlass =
      quadrance noise' < 2/3

    (height, color, lights, roughness) =
      if isGlass then
        ( 0.995 + 0.005 * alpha'
        , surface ^* 0.5
        , 0
        , 0.85
        )
      else
        ( 1.0 + 0.01 * (alpha + beta + gamma) / 3
        , surface
        , max 0 $ stars normPos (const 10000 details) ^* 0.25
        , 0.99
        )

  pure
    ( Vec3.Packed $ normPos ^* height
    , LitColored.VertexAttrs
        { vaBaseColor         = Vec4.fromVec3 color 1
        , vaEmissiveColor     = Vec4.fromVec3 lights 1
        , vaMetallicRoughness = vec2 0 roughness
        , vaNormal            = Vec3.Packed normPos
        }
    )
  where
    colorA = vec3 1 1 1
    colorB = vec3 1 0 0
    colorC = vec3 1 1 0

    -- colorA = vec3 0 0.2 1.0
    -- colorB = vec3 0 0.5 0.2
    -- colorC = vec3 0 0.2 0.0

stars :: Vec3 -> Float -> Vec3
stars p res = (c * c) ^* 0.8
  where
    p2 = p ^* (0.15 * res)
    q = fract3 p2 - 0.5
    WithVec3 noise0 noise1 _noise2 = hash33 (floor3 p2)
    cStart = 1.0 - smoothstep 0 0.6 (norm q)

    c = foldl' cf (vec3 0 0 0) ([0,1,2,3] :: [Float])
      where
        cf :: Vec3 -> Float -> Vec3
        cf acc i = acc + cStep ^* c2
          where
            c2 =
              cStart * step noise0 (0.0005 + i * i * 0.001)
            cStep =
              ( linear
                  (vec3 1.0 0.49 0.1)
                  (vec3 0.75 0.9 1.0)
                  noise1
                ^* 0.1
              ) + 0.9

smoothstep :: (Ord a, Fractional a) => a -> a -> a -> a
smoothstep a b x =
  let
    t = saturate ((x-a) / (b-a))
  in
    t * t * (3 - 2 * t)

step :: (Ord a, Num p) => a -> a -> p
step a x =
  if x < a then
    0
  else
    1

clamp :: Ord a => a -> a -> a -> a
clamp x a b = min (max x a) b

saturate :: (Ord a, Num a) => a -> a
saturate x = clamp x 0 1

fract3 :: Vec3 -> Vec3
fract3 v =
  withVec3 v \a b c ->
    vec3 (fract a) (fract b) (fract c)

floor3 :: Vec3 -> Vec3
floor3 v =
  withVec3 v \a b c ->
    vec3
      (fromIntegral @Int $ floor a)
      (fromIntegral @Int $ floor b)
      (fromIntegral @Int $ floor c)

hash33 :: Vec3 -> Vec3
hash33 input =
  withVec3 (p + vec3 p' p' p') \x y z ->
    vec3
      (fract $ x * y)
      (fract $ z * x)
      (fract $ y * z)
  where
    seed = vec3 443.8975 397.2973 491.1871

    p =
      withVec3 (input * seed) \x y z ->
        vec3 (fract x) (fract y) (fract z)

    p' =
      withVec3 p \x y z ->
        dot
          (vec3 z x y)
          (vec3 y x z + 19.27)
