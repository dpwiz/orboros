{-# LANGUAGE OverloadedLists #-}

module Stage.Main.Render
  ( updateBuffers
  , recordCommands
  ) where

import RIO

import Engine.Types qualified as Engine
import Engine.Vulkan.DescSets (withBoundDescriptorSets0)
import Engine.Vulkan.Pipeline.Graphics qualified as Graphics
import Engine.Vulkan.Swapchain qualified as Swapchain
import Render.Basic qualified as Basic
import Render.DescSets.Set0 qualified as Set0
import Render.Draw qualified as Draw
import Render.ForwardMsaa qualified as ForwardMsaa
import Render.ShadowMap.RenderPass qualified as ShadowPass
import RIO.State (gets)
import Vulkan.Core10 qualified as Vk

import Stage.Main.Types (FrameResources(..), RunState(..))
import Stage.Main.Resource.Model qualified as Model
import Stage.Main.UI qualified as UI
import Stage.Main.World qualified as World

updateBuffers
  :: RunState
  -> FrameResources
  -> Basic.StageFrameRIO FrameResources RunState ()
updateBuffers RunState{..} FrameResources{..} = do
  Set0.observe rsSceneP frScene
  Set0.observe rsSceneUiP frSceneUi

  World.observe rsOrbitSim frWorld

  UI.observe rsUI frUI

recordCommands
  :: Vk.CommandBuffer
  -> FrameResources
  -> Word32
  -> Basic.StageFrameRIO FrameResources RunState ()
recordCommands cb FrameResources{..} imageIndex = do
  (_context, Engine.Frame{fSwapchainResources, fRenderpass, fPipelines}) <- ask
  let Basic.Pipelines{..} = fPipelines

  Model.Collection{..} <- mapRIO fst $ gets rsModels

  (messages, topMessageBg, messageLogBg) <- UI.readObserved frUI
  World.WorldBuffers{..} <- World.readObserved frWorld

  -- updateShadow <- stageFrameGetRS rsUpdateShadow
  -- Worker.observeIO_ updateShadow frUpdateShadow \() () -> do
  when True do
    let shadowLayout = Graphics.pLayout pShadowCast
    ShadowPass.usePass (Basic.rpShadowPass fRenderpass) imageIndex cb do
      let shadowArea = ShadowPass.smRenderArea $ Basic.rpShadowPass fRenderpass
      Swapchain.setDynamic cb shadowArea shadowArea
      withBoundDescriptorSets0 cb Vk.PIPELINE_BIND_POINT_GRAPHICS shadowLayout frSunDescs $
        Graphics.bind cb pShadowCast do
          Draw.indexedPos cb planet planetInst
          -- Draw.indexedPos cb playerHead playerHeads
          -- Draw.indexedPos cb playerTail playerTails
          -- Draw.indexedPos cb cargoPod cargoPods
          -- Draw.indexedPos cb fuelPod fuelPods

  ForwardMsaa.usePass (Basic.rpForwardMsaa fRenderpass) imageIndex cb do
    Swapchain.setDynamicFullscreen cb fSwapchainResources

    Set0.withBoundSet0 frScene pLitColored cb do
      Graphics.bind cb pLitColored do
        Draw.indexed cb planet planetInst

        Draw.indexed cb playerHead playerHeads
        Draw.indexed cb playerTail playerTails
        Draw.indexed cb cargoPod cargoPods
        Draw.indexed cb fuelPod fuelPods

      Graphics.bind cb pWireframe do
        Draw.indexed cb altitudeBars zeroTransform
        Draw.indexed cb trajectories zeroTransform

      Graphics.bind cb pSkybox $
        Draw.triangle_ cb

    Set0.withBoundSet0 frSceneUi pUnlitColored cb do
      Graphics.bind cb pUnlitColored do
        Draw.indexed cb shroud topMessageBg
        Draw.indexed cb shroud messageLogBg

      Graphics.bind cb pEvanwSdf $
        traverse_ (Draw.quads cb) messages

    -- ImGui.draw dear cb
