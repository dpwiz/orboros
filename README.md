# Orbital rendezvous objectives simulator

> It's a snake! On orbital planes!

Originally, this is a no-engine game based on raw Vulkan SDK and some Haskell libraries I made to load stuff.
Current version is a feature-complete [Keid] engine rewrite.

[Keid]: https://keid.haskell-game.dev/

At least the Linux build should work out of the box on anything older than Ubuntu 18.

The gameplay is a Snake - you move around and collect stuff.
The twist is the playing field is actually 6-dimensional, as in [6 orbital elements](https://en.wikipedia.org/wiki/Orbital_elements). See below for controls.

![Capture d’écran de 2021-07-17 12-19-13.png](https://i.imgur.com/ifsRCBw.png)

## Controls

The controls are **not** intuitive. I had difficulties with rendezvous missions in Kerbal Space Program so I made this to grok it. It should be even more difficult with limited fuel, time and other restrictions.

The controls are two-fold:

* To actually change your orbit, activate (and hold) `[space]`.
* To lock spacecraft orientation use:
  * `[W]` - orient prograde. That is, towards your next position, "forward". This will raise the opposite point of your orbit.
  * `[S]` - orient retrograde. Turn around, engine-forward and prepare to brake. This will lower the point at 180° around your orbit.
  * `[A]`/`[D]` - orient "inside" and "outside" orbit. This is where things get funky. Those are slow burns that shift your orbit around.
  * `[Q]`/`[E]` - orient perpendicularly to your orbital plane. This will adjust your inclination, however unintuitively. Even slower burn.

That's it. Things will get faster the closer they get to the planet.
Red color indicates the greater speed and lower altitude.
Perfecly circular orbits have pure blue color without any speed gradients.

### Extras

* `[←]`/`[→]`/`[↑]`/`[↓]` - rotate camera around the planet to get a better look.
* `[+]`/`[-]` - adjust camera distance.
* `[0]`/`[P]` - toggle game time (pause).
<!--
* `[1]`/`[2]` - spawn more containers to catch!
* `[Backspace]` - jettison your score and yourself to restart.
-->

# Program options

* `--help` - show actual options available (this list can be out of date)
* `--version` - show build version and git revision
* `--validation` - use Khronos validation layers
* `--verbose` - show more messages
* `--fullscreen` - run fullscreen on a largest display

# Building from source

## Git LFS

> ⚠ This repository uses [git large file storage](https://git-lfs.github.com/).

If you see errors like "unrecognized audio format", check file sizes inside `resources/` and if they aren't in the megabytes range you have to install git LFS:

```shell
sudo apt install git-lfs # on Ubuntu
git lfs install
```

Then, clone the repo again.

## Haskell

I use stack to build, but this shouldn't matter much.
You can convert it to cabal-v2 or nix project (and send a patch).

```shell
stack run
```

## Libraries

Get Vulkan SDK at https://vulkan.lunarg.com/sdk/home.

```shell
sudo apt install libopenal-dev libglfw3-dev libopusfile-dev vulkan-sdk
```
